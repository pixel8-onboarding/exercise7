# Exercise Task No. 7:
TEN 7 | PHP & MYSQL | Create Simple CRUD

Based on the previous tasks "PHP | Basic PHP Input Handling" and "MYSQL | Basic MySQL Queries," create a CRUD functionality that integrates the two (2) Previous Tasks.



# PHP MySQL CRUD Operations

This project demonstrates basic CRUD (Create, Read, Update, Delete) operations using PHP and MySQL without using HTML forms. The project focuses on managing a simple `products` table in a MySQL database.

## Table Structure

The `products` table has the following structure:

- `id` (int, AUTO_INCREMENT): The primary key for the table.
- `name` (varchar(255)): The name of the product.
- `description` (varchar(255)): A brief description of the product.
- `price` (decimal(10,2)): The price of the product.
- `quantity` (int): The quantity of the product in stock.

The SQL script to create the `products` table is as follows:

```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `quantity` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;