<?php 

//Connect Database:
$host = 'localhost';
$username = 'root';
$password ='';
$database = 'exercise7';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection Failed:" . $conn->connect_error);
}

//function to add a product

function createProduct($conn, $name, $description, $price, $quantity){

    $sql = "INSERT INTO products (name, description, price, quantity) 
    VALUES ('$name', '$description' , '$price', '$quantity')";


    if ($conn->query($sql)=== TRUE){ 
        echo "New products adedd successfully \n";

    }else{
        echo "Error: " . $sql . "\n" . $conn->error;
    }


}

//function to retrieve all products
function getAllProduct($conn){
    $sql = "SELECT name, description, price, quantity FROM products";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo " - Name: " . $row["name"]. " - Description: " . $row["description"]. 
                 " - Price: $" . $row["price"]. " - Quantity: " . $row["quantity"]. "\n";
        }
    } else {
        echo "0 results\n";
    }


}

//get a product price by its name
function getProductPriceByName($conn, $name) {
    $sql = "SELECT price FROM products WHERE name = '$name'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "Price of $name: $" . $row["price"] . "\n";
    } else {
        echo "Product not found\n";
    }
}

//product update by id
function updateProduct($conn, $id, $price, $quantity) {
    $sql = "UPDATE products SET price = $price, quantity = $quantity WHERE id = $id";
    
    if ($conn->query($sql) === TRUE) {
        echo "Product updated successfully\n";
    } else {
        echo "Error updating product: " . $conn->error;
    }
}


// Function to delete a product by id
function deleteProduct($conn, $id) {
    $sql = "DELETE FROM products WHERE id = $id";
    
    if ($conn->query($sql) === TRUE) {
        echo "Product deleted successfully\n";
    } else {
        echo "Error deleting product: " . $conn->error;
    }
}


// Create a new product
createProduct($conn, 'Product A', 'Description of Product A', 19.99, 100);
createProduct($conn, 'Product B', 'Description of Product A', 7.90, 50);

// Read all products
echo "Reading all products:\n";
getAllProduct($conn);



// Update a product
updateProduct($conn, 1, 24.99, 120);
// Read all products after update
echo "Reading all products after update:\n";
getAllProduct($conn);

// Get product price by name
getProductPriceByName($conn, 'Product A');

// Delete a product by id
deleteProduct($conn, 1);

// Read all products after deletion
echo "Reading all products after deletion:\n";
getAllProduct($conn);

$conn->close();

?>